import tensorflow as tf
import cv2
import numpy as np

import svhn_data as data
import svhn_model_v3 as model

import color_selector as cs
import target_selector as ts
import number_divider as nd
import augmenter as aug

DATA_PATH = '/home/fireflash/Desktop/Machine_Learning/TestProjects/numbers'
MODEL_PATH = 'model_test.ckpt'
IMG_SIZE = 32

image_path = '7.png'

def _normalize(image):
    y_len, x_len = image.shape
    max_color = 255
    
    for y in range(0, y_len):
        for x in range(0, x_len):
            image[y, x] = (image[y, x] - (int)(max_color / 2))
            
def _digit_from_indexes(indexes):
    digit = ""
    
    for index in indexes:
        if (index == 9):
            digit = (digit) + str(0)
        else:
            digit = (digit) + str(index + 1)
            
    return digit

def preprocesed_nums(image_path, DATA_PATH):
    image_colored = data.image_colored(image_path, DATA_PATH)
    image_grayscale = data.image_grayscale(image_path, DATA_PATH)
    
    targeted_segm = ts.targeted_segmented_to_minimum(image_colored)
    targeted_colored = ts.targeted_colored(ts.segmented_grayscale(image_grayscale),
                                           image_colored)
    targeted_gray = cv2.cvtColor(targeted_colored, cv2.COLOR_BGR2GRAY)
    
    main_color, bg_color = cs.get_color(targeted_segm)
    num_images, borders = nd.separated_numbers(targeted_segm, targeted_gray,
                                      main_color, bg_color)
    aug_images = aug.augmented_grayscale_numbers(num_images, borders,
                                    [IMG_SIZE, IMG_SIZE], targeted_segm, bg_color)
    
    #aug_images = data.normalize_data(aug_images)
    aug_images = aug_images.reshape((-1, IMG_SIZE * IMG_SIZE)).astype(np.float32)
    
    return aug_images


test_dataset, test_labels = data.create_test_dataset()
test_dataset, test_labels = test_dataset[:3], test_labels[:3]

def image_nums_estimation():
    preproc_nums = preprocesed_nums(image_path, DATA_PATH)
    
    
    with tf.Session(graph=model.model_graph()) as sess:
        model.saver.restore(sess, MODEL_PATH)
        digit = None

        estimate = model.y_conv.eval(feed_dict={
                model.x: preproc_nums, model.keep_prob: 1.0})
        indexes = np.argmax(estimate, 1)
        digit = _digit_from_indexes(indexes)
    
    return digit

def main():
    print(image_nums_estimation())
    

if __name__ =="__main__":main()
        
    
    
    
    
    
    
    
    
    
    
    
    