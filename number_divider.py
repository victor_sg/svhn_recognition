import numpy as np
import math


def _clean_background_parts(targeted_segm, num_color, bg_color):
    y_len, x_len = targeted_segm.shape
    
    #top
    #bottom
    #left
    #right
    pass

def clean_the_noise(targeted_segm, num_color, bg_color):
    pass

def _number_count_per_row(targeted_segm, num_color, bg_color,
                          zeros_included=False):
    WIDTH_COEF = 0.035
    
    y_len, x_len = targeted_segm.shape
    number_part_width = (int)(x_len * WIDTH_COEF)
    parts_per_row = []
    number_borders_per_row = []
    
    for y in range(0, y_len):
        parts = 0
        prev_color = 0
        num_part_count = 0
        start_x = 0
        
        number_borders = []
        
        #check the first element/part of the row
        for check_x in range(0, x_len):
            if (targeted_segm[y, check_x] == bg_color):
                prev_color = bg_color
                start_x = check_x
                break
            elif (targeted_segm[y, check_x] == num_color):
                start_x = check_x
                prev_color = num_color
        
        for x in range(start_x, x_len):
            if (prev_color == bg_color):
                if (targeted_segm[y, x] == bg_color):
                    continue
                elif (targeted_segm[y, x] == num_color):
                    prev_color = num_color
                    num_part_count = num_part_count + 1
                    
            elif (prev_color == num_color):
                if (targeted_segm[y, x] == bg_color):
                    if (num_part_count > number_part_width):
                        number_borders.append(x - num_part_count)
                        number_borders.append(x - 1)
                        
                        parts = parts + 1
                        num_part_count = 0
                        prev_color = bg_color
                    else: 
                        num_part_count = 0
                        prev_color = bg_color
                        
                elif (targeted_segm[y, x] == num_color):
                    num_part_count = num_part_count + 1
        
        if (len(number_borders) > 0):
            overal_borders = []
            overal_borders.append(number_borders[0])
            overal_borders.append(number_borders[-1])
            number_borders_per_row.append(overal_borders)
        
        if (zeros_included == True):
            parts_per_row.append(parts)
        elif (parts != 0):
            parts_per_row.append(parts)
            
    return parts_per_row, number_borders_per_row

def _sepatated_number_with_margin(gray_targ, borders):
    MARGIN_PART = 0.2
    
    y_len, x_len = gray_targ.shape
    distance_btw_borders = borders[1] - borders[0]
    margin = round(distance_btw_borders * MARGIN_PART)
    number_with_margin = None
    
    borders[0] -= margin
    borders[1] += margin
    
    #check if not out of image's indexes
    while (borders[0] < 0):
            borders[0] += 1

    while (borders[1] >= x_len):
            borders[1] -= 1
    
    number_with_margin = gray_targ[:, (int)(borders[0]):(int)(borders[1])] 
    
    return number_with_margin, borders
                    
def number_count(count_per_row):
    THRESHOLD = 0.2
    
    number_count = 0
    floating_number_count = np.average(count_per_row)
    
    if (floating_number_count % 1 < THRESHOLD):
        number_count = (int)(floating_number_count)
    elif (floating_number_count % 1 >= THRESHOLD):
        number_count = (int)(floating_number_count) + 1
    else:
        number_count = 0
        
    return number_count

def separated_numbers(targeted_segm, targeted_gray, num_color, bg_color):
    count_per_row, number_borders_per_row = _number_count_per_row(
            targeted_segm, num_color, bg_color)
    
    aprox_count = number_count(count_per_row)
    aprox_overall_borders = np.mean(number_borders_per_row, axis=0)
    ready_num_borders = np.zeros((aprox_count, 2))
    separated_images = []
    borders_with_margin = []
    
    aprox_overall_borders[0] = math.floor(aprox_overall_borders[0])
    aprox_overall_borders[1] = math.ceil(aprox_overall_borders[1])
    
    overall_distance = aprox_overall_borders[1] - aprox_overall_borders[0]
    num_distance = overall_distance / aprox_count
    
    #getting non-floating distance
    front = True
    while (num_distance % 1 != 0):
        if (front == True):
            aprox_overall_borders[1] += 1
            overall_distance = aprox_overall_borders[1] - aprox_overall_borders[0]
            num_distance = overall_distance / aprox_count
            front = False
            
        elif (front == False):
            aprox_overall_borders[0] -= 1
            overall_distance = aprox_overall_borders[1] - aprox_overall_borders[0]
            num_distance = overall_distance / aprox_count
            front = True
    
    #getting each number X-axis border indexes
    start = aprox_overall_borders[0]
    distance_btw_borders = num_distance - 1
    for num_index in range(0, aprox_count):
        end = start + distance_btw_borders
        ready_num_borders[num_index, 0] = start
        ready_num_borders[num_index, 1] = end
        start = end + 1
        
    for num_borders in ready_num_borders:
        num_image, borders = _sepatated_number_with_margin(targeted_gray, num_borders)
        separated_images.append(num_image)
        borders_with_margin.append(borders)
        
    
    return np.array(separated_images), np.array(borders_with_margin)
    




















