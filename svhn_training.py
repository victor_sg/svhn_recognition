import tensorflow as tf
import svhn_data as data
from matplotlib import pyplot as plt
import svhn_model_v3 as model


NUM_STEPS = 200
BATCH_SIZE = 250

train_dataset, train_labels = data.create_training_dataset()
valid_dataset, valid_labels = data.create_validation_dataset()
test_dataset, test_labels = data.create_test_dataset()

train_dataset, train_labels = train_dataset[:5000], train_labels[:5000]
valid_dataset, valid_labels = valid_dataset[:2000], valid_labels[:2000]
test_dataset, test_labels = test_dataset[:3000], test_labels[:3000]

train_results = []
valid_results = []
steps = []

with tf.Session(graph=model.model_graph()) as sess:
    sess.run(tf.global_variables_initializer())
    for step in range(NUM_STEPS):
        offset = (step * BATCH_SIZE) % (train_labels.shape[0] - BATCH_SIZE)
        
        batch_data = train_dataset[offset:(offset + BATCH_SIZE)]
        batch_labels = train_labels[offset:(offset + BATCH_SIZE)]
        
        if step % 10 == 0:
            train_accuracy = model.accuracy.eval(feed_dict={
                    model.x: batch_data, model.y_: batch_labels, model.keep_prob: 1.0})
            print('Step %d, training accuracy: %f' % (step, train_accuracy))
            
            valid_accuracy = model.accuracy.eval(feed_dict={
                    model.x: valid_dataset, model.y_: valid_labels, model.keep_prob: 1.0})
            print('Valid accuracy %g' % valid_accuracy)
            
            train_results.append(train_accuracy)
            valid_results.append(valid_accuracy)
            steps.append(step)
            
        model.train_step.run(feed_dict={model.x: batch_data, model.y_: batch_labels,
                                        model.keep_prob: model.DROPOUT})
    test_accuracy = model.accuracy.eval(feed_dict={
                    model.x: test_dataset, model.y_: test_labels, model.keep_prob: 1.0})
    print('Test accuracy %g' % test_accuracy)
    
    save_path = model.saver.save(sess, 'model_test.ckpt')
    print("Model saved in file: %s" % save_path)

    

axes = plt.gca()
axes.set_ylim([0, 1.0])
plt.plot(steps, train_results, c='r', linewidth=2)
plt.plot(steps, valid_results, c='b', linewidth=2)
plt.show()
