import numpy as np


def _get_main_colors(segmented_image):
    colors = []
    y_len, x_len = segmented_image.shape
    
    for y in range(0, y_len):
        for x in range(0, x_len):
            color = segmented_image[y, x]
            if (color not in colors):
                colors.append(color)
                
    if (len(colors) == 1):
        colors.append(colors[0])
    
    return colors

def _get_background_color(first_color, poles_first_color_count,
                              sides_first_color_count, second_color,
                              poles_second_color_count, sides_second_color_count):
    poles_main_color = None
    sides_main_color = None
    
    if (poles_first_color_count > poles_second_color_count):
        poles_main_color = first_color
    else: poles_main_color = second_color
    
    if (sides_first_color_count > sides_second_color_count):
        sides_main_color = first_color
    else: sides_main_color = second_color
    
    if (poles_main_color == sides_main_color):
        return poles_main_color
    
    #This part could be significantly upgraded using separate NN to calculate score,
    #instead of just considering mere color_countecond_color_count
    #statistics is the vital part here
    
    return poles_main_color
    

def _color_by_frames(segm_targeted_img, percentage=0.2):
    sides_section_width = (int)(segm_targeted_img.shape[1] * percentage)
    poles_section_height = (int)(segm_targeted_img.shape[0] * percentage)
    y_len, x_len = segm_targeted_img.shape
    
    first_color, second_color = _get_main_colors(segm_targeted_img)
    
    poles_first_color_count = 0
    poles_second_color_count = 0
    sides_first_color_count = 0
    sides_second_color_count = 0
    
    result_color = None
    
    #top pole
    border_y = 0
    for x in range(0, x_len):
        if (x < poles_section_height):
            border_y = x
        elif (x > x_len - poles_section_height):
            border_y = border_y - 1
        else: border_y = poles_section_height
        
        for y in range(0, border_y):
            if (segm_targeted_img[y, x] == first_color):
                poles_first_color_count = poles_first_color_count + 1 
            else: poles_second_color_count = poles_second_color_count + 1
            
    #bottom pole
        border_y = 0
    for x in range(0, x_len):
        if (x < poles_section_height):
            border_y = y_len - x
        elif (x > x_len - poles_section_height):
            border_y += 1
        else: border_y = y_len - poles_section_height
        
        for y in range(0, border_y):
            if (segm_targeted_img[y, x] == first_color):
                poles_first_color_count += 1
            else: poles_second_color_count += 1
            
    #left side
        border_x = 0
    for y in range(0, y_len):
        if (y < sides_section_width):
            border_x = y
        elif (y > y_len - sides_section_width):
            border_x -= 1
        else: border_x = sides_section_width
        
        for x in range(0, border_x):
            if (segm_targeted_img[y, x] == first_color):
                sides_first_color_count += 1
            else: sides_second_color_count += 1
            
    #right side
        border_x = 0
    for y in range(0, y_len):
        if (y < sides_section_width):
            border_x = x_len - x
        elif (y > y_len - sides_section_width):
            border_x += 1
        else: border_x = x_len - sides_section_width
        
        for y in range(0, border_x):
            if (segm_targeted_img[y, x] == first_color):
                sides_first_color_count += 1
            else: sides_second_color_count += 1
            
    background_color = _get_background_color(first_color, poles_first_color_count,
                              sides_first_color_count, second_color,
                              poles_second_color_count, sides_second_color_count)
    if (background_color == first_color):
        result_color = second_color
    else: result_color = first_color
    
    return result_color, background_color
            
def get_color(segm_targeted_img):
    num_color, bg_color = _color_by_frames(segm_targeted_img)
    #here goes correction of result / checking methods if the color was
    #selected properly
    return num_color, bg_color
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            