import numpy as np
import cv2
from matplotlib import pyplot as plt


def _reshape_numbers(image, shape):
    image_rows, image_cols = image.shape
    new_side, _ = shape
    
    bigger_axis = None
    smaller_axis = None
    axis_ratio = None
    reshaped_image = image
    
    if (image_rows > image_cols):
        bigger_axis = image_rows
        smaller_axis = image_cols
    else:
        bigger_axis = image_cols
        smaller_axis = image_rows
        
    axis_ratio = bigger_axis / new_side
    
    if (bigger_axis == image_cols):
        reshaped_image = cv2.resize(image, (new_side, (int)(smaller_axis / axis_ratio)))
    else:
        reshaped_image = cv2.resize(image, ((int)(smaller_axis / axis_ratio), new_side))
        
    return reshaped_image
    

def _color_for_image_augmentation(image, borders, targeted_segm, segm_bg_color):
        #selecting segmented part of given image
    x_min, x_max = borders
    segm_image = targeted_segm[:, (int)(x_min):(int)(x_max)]
    y_len, x_len = segm_image.shape
    bg_index = np.zeros((2), dtype=np.int)
    
    bg_aug_color = None
    bg_found = False
    
    for y in range(0, y_len):
        for x in range(0, x_len):
            if (segm_image[y, x] == segm_bg_color):
                bg_index[0] = y
                bg_index[1] = x
                bg_found =True
                break

        if(bg_found == True):
            break
    
    bg_aug_color = image[bg_index[0], bg_index[1]]
    return bg_aug_color

def _augment_number(image, borders, shape,
                     targeted_segm, segm_bg_color):
    
    bg_aug_color = _color_for_image_augmentation(image, borders,
                                                 targeted_segm, segm_bg_color)
    
    reshaped_image = _reshape_numbers(image, shape)
    side, _ = shape
    y_len, x_len = reshaped_image.shape
    aug_image = np.full((side, side), bg_aug_color)
    
    if (y_len == side):
        distance = side - x_len
        x_start = (int)(distance / 2)
        for y in range(0, y_len):
            for x in range(0, x_len):
                aug_image[y, x + x_start] = reshaped_image[y, x]
                
    elif (x_len == side):
        distance = side - y_len
        y_start = (int)(distance / 2)
        for y in range(0, y_len):
            for x in range(0, x_len):
                aug_image[y + y_start, x] = reshaped_image[y, x]
    else: print("fail")
                
    return aug_image
    

def augmented_grayscale_numbers(images, borders, shape,
                     targeted_segm, segm_bg_color):
    
    num_count = len(images)
    aug_images = []
    
    for num_index in range(0, num_count):
        image = images[0]
        image_borders = borders[0]
        aug_image = _augment_number(image, image_borders, shape,
                     targeted_segm, segm_bg_color)
        aug_images.append(aug_image)
        
    return np.array(aug_images)
        
























