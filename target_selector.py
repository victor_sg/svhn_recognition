import numpy as np
from skimage import io, segmentation as seg, color
import cv2


def _target_area_Xs(segmented_image):
    center_X = (int)(segmented_image.shape[1] / 2)
    center_Y = (int)(segmented_image.shape[0] / 2)
    center_color = segmented_image[center_Y, center_X]
    
    X_min = center_X
    X_max = center_X
    
    for y in range(segmented_image.shape[0]):
        for x in range(segmented_image.shape[1]):
            if segmented_image[y, x] != center_color:
                continue
            if x < X_min:
                X_min = x
            elif x > X_max:
                X_max = x
    
    return [X_min, X_max]

def _mean_color(image, labels):
    out = np.zeros_like(image)
    
    for label in np.unique(labels):
        indices = np.nonzero(labels == label)
        out[indices] = np.mean(image[indices], axis=0)
    return out

def segmented_grayscale(grayscale_image, n_segments=4):
    labels = seg.slic(grayscale_image, n_segments=n_segments, compactness=1, 
                  enforce_connectivity=False, sigma=3.75)
    labels_image = _mean_color(grayscale_image, labels)
    return labels_image

def targeted_colored(segmented_gray, colored_img):
    x_min, x_max = _target_area_Xs(segmented_gray)
    targeted_img = colored_img[:, x_min:x_max]
    
    return targeted_img
    
def targeted_segmented_to_minimum(colored_img):
    grayscale_img = cv2.cvtColor(colored_img, cv2.COLOR_BGR2GRAY)
    segmented_gray = segmented_grayscale(grayscale_img)
    
    targeted_img = targeted_colored(segmented_gray, colored_img)
    
    labels = seg.slic(targeted_img, n_segments=2, compactness=1,
                  enforce_connectivity=False, sigma=1)
    labels_image = _mean_color(targeted_img, labels)
    labels_image = cv2.cvtColor(labels_image, cv2.COLOR_BGR2GRAY)
    return labels_image
    

















