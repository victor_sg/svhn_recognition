import tensorflow as tf


NUM_LABELS = 10
NUM_CHANNELS = 1
IMAGE_SIZE = 32

DEPTH_1 = 32
DEPTH_2 = 64
DEPTH_3 = 128
DEPTH_4 = 256
DENSE = 1024
KERNEL_SIZE = 5
DROPOUT = 0.85

DECAY = 0.96
STEPS_TO_DECAY = 100
START_LEARN_RATE = 1e-5


def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1],
                          padding='SAME')


graph = tf.Graph()

with graph.as_default():
    x = tf.placeholder(tf.float32, shape=(None, IMAGE_SIZE * IMAGE_SIZE), name='input')
    y_ = tf.placeholder(tf.float32, shape=(None, NUM_LABELS), name='labels')
    
    x_image = tf.reshape(x, [-1, IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS])
        
    W_conv1 = weight_variable([KERNEL_SIZE, KERNEL_SIZE, NUM_CHANNELS, DEPTH_1])
    b_conv1 = bias_variable([DEPTH_1])
    h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
    h_pool1 = max_pool_2x2(h_conv1)
    
    W_conv2 = weight_variable([KERNEL_SIZE, KERNEL_SIZE, DEPTH_1, DEPTH_2])
    b_conv2 = bias_variable([DEPTH_2])
    h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
    h_pool2 = max_pool_2x2(h_conv2)
    
    W_conv3 = weight_variable([KERNEL_SIZE, KERNEL_SIZE, DEPTH_2, DEPTH_3])
    b_conv3 = bias_variable([DEPTH_3])
    h_conv3 = tf.nn.relu(conv2d(h_pool2, W_conv3) + b_conv3)
    h_pool3 = max_pool_2x2(h_conv3)
    
    W_conv4 = weight_variable([KERNEL_SIZE, KERNEL_SIZE, DEPTH_3, DEPTH_4])
    b_conv4 = bias_variable([DEPTH_4])
    h_conv4 = tf.nn.relu(conv2d(h_pool3, W_conv4) + b_conv4)
    h_pool4 = max_pool_2x2(h_conv4)
    
    layer_shape = h_pool3.get_shape()
    num_features = layer_shape[1:4].num_elements()
    W_fc1 = weight_variable([num_features, DENSE])
    b_fc1 = bias_variable([DENSE])
    
    h_pool2_flat = tf.reshape(h_pool3, [-1, num_features])
    h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
    
    keep_prob = tf.placeholder(tf.float32)
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
    
    W_fc2 = weight_variable([DENSE, NUM_LABELS])
    b_fc2 = bias_variable([NUM_LABELS])
    
    y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2
    
    cross_entropy = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(
                    labels=y_, logits=y_conv))
    
    global_step = tf.Variable(0, trainable=False)
    learning_rate = tf.train.exponential_decay(START_LEARN_RATE, global_step,
                                               STEPS_TO_DECAY, DECAY, staircase=True)
    train_step = tf.train.AdamOptimizer(learning_rate).minimize(cross_entropy)
    correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    saver = tf.train.Saver()
    
def model_graph():
    return graph










