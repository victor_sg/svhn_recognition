import cv2
import numpy as np
import os
import scipy.io as sio


TRAIN_PATH = '/home/fireflash/Desktop/Machine_Learning/SVHN/data/train_32x32.mat'
TEST_PATH = '/home/fireflash/Desktop/Machine_Learning/SVHN/data/test_32x32.mat'
IMAGE_SIZE = 32


def create_training_dataset(validation_portion=0.3):
    dataset, labels = _load_data(TRAIN_PATH)
    threshold = (int)(dataset.shape[3] * 0.3)
    dataset = dataset[:, :, :, threshold:]
    labels = labels[threshold:]
    
    dataset = _grayscale_dataset(dataset)
    #dataset = normalize_data(dataset)
    dataset, labels = _reformat_data(dataset, labels)
    return dataset, labels

def create_validation_dataset(validation_portion=0.3):
    dataset, labels = _load_data(TRAIN_PATH)
    threshold = (int)(dataset.shape[3] * 0.3)
    dataset = dataset[:, :, :, :threshold]
    labels = labels[threshold:]
    
    dataset = _grayscale_dataset(dataset)
    #dataset = normalize_data(dataset)
    dataset, labels = _reformat_data(dataset, labels)
    return dataset, labels

def create_test_dataset():
    dataset, labels = _load_data(TEST_PATH)
    dataset = _grayscale_dataset(dataset)
    #dataset = normalize_data(dataset)
    dataset, labels = _reformat_data(dataset, labels)
    return dataset, labels

def image_grayscale(image_path, DATA_PATH, divider=1):
    path = os.path.join(DATA_PATH, image_path)
    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(img, ((int)(img.shape[1] / divider), (int)(img.shape[0] / divider)))
    return img

def image_colored(image_path, DATA_PATH):
    path = os.path.join(DATA_PATH, image_path)
    img = cv2.imread(path, cv2.IMREAD_COLOR)
    return img

def normalize_data(dataset):
    return ((dataset - 127.5) / 255.0).astype(np.float32)
    
def _load_data(path):
    data = sio.loadmat(path)
    return data['X'], data['y']

def _grayscale_dataset(dataset):
    gray_dataset = []
    for index in range(dataset.shape[3]):
        gray_image = cv2.cvtColor(dataset[:, :, :, index], cv2.COLOR_BGR2GRAY)
        gray_dataset.append(gray_image)
    return np.array(gray_dataset)

def _reformat_data(dataset, labels):
    dataset = dataset.reshape((-1, IMAGE_SIZE * IMAGE_SIZE)).astype(np.float32)
    labels = labels.reshape(-1).astype(np.float32)
    labels = (np.arange(10) + 1 == labels[:, None]).astype(np.float32)
    return dataset, labels
